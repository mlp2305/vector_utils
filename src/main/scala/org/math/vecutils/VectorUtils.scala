package org.math.vecutils

import javax.vecmath.Vector3f
import javax.vecmath.Matrix3f
import javax.vecmath.Vector2f

object VectorUtils {
  def Vector(scale:Float,x:Float,y:Float,z:Float) = new Vector3f(x*scale,y*scale,z*scale)
  def Vector(x:Float,y:Float,z:Float) = new Vector3f(x,y,z)
  /*def mul(m:Matrix3f, v:Vector3f):Vector3f = {
    var matV = vecAsMat(v)
    matV.mul(m, matV)
    matAsVec(m)
  }*/ 
  def cross(v1:Vector3f,v2:Vector3f)  = {
    val v = new Vector3f();
    v.cross(v1,v2);
    v;
  }
  
  def divide(v1:Vector3f,v2:Vector3f)  = {
    val v = new Vector3f(v1);
    v.x = v.x/v2.x;
    v.y = v.y/v2.y;
    v.z = v.z/v2.z;
    v
  }
  def mul(v1:Vector3f,v2:Vector3f)  = {
    val v = new Vector3f(v1);
    v.x = v.x*v2.x;
    v.y = v.y*v2.y;
    v.z = v.z*v2.z;
    v
  }
  def getColumn(m:Matrix3f,c:Int) = {
    val v = new Vector3f();
    m.getColumn(c,v);
    v;
  }
  def getRow(m:Matrix3f,r:Int) = {
    val v = new Vector3f();
    m.getRow(r,v);
    v;
  }
  
  def add(v1:Vector3f, v2:Vector3f):Vector3f = {
    val v = new Vector3f(v1)
    v.add(v2)
    v
  }
  def sub(v1:Vector3f, v2:Vector3f):Vector3f = {
    val v = new Vector3f(v1)
    v.sub(v2)
    v
  }
  def negate(v:Vector3f):Vector3f = {
    var v_ = new Vector3f(v);
    v_.negate();
    v_
  }
  def negate(v:Vector2f):Vector2f =  {
    var v_ = new Vector2f(v);
    v_.negate();
    v_;
  }
  def sub(v1:Vector2f, v2:Vector2f):Vector2f = {
    val v = new Vector2f(v1)
    v.sub(v2)
    v
  }
  def normalize(v:Vector3f) =  {
    val v_ = new Vector3f(v);
    v_.normalize();
    v_;
  }
  def getHighestDot(v1:Vector3f,v2:Vector3f) = {
    var dot1 = math.abs(v1.dot(v2));
    var dot2 = math.abs(v1.dot(negate(v2)));
    math.max(dot1,dot2).asInstanceOf[Float]
  }

  def mul(m1:Matrix3f,m2:Matrix3f) = {
    var r = new Matrix3f(m1);
    r.mul(m2);
    r
  }
  def eulerToMat(rX:Float, rY:Float, rZ:Float) = {
    var m = new Matrix3f()
    m.setIdentity()
    rotX(m, rX)
    rotY(m, rY)
    rotZ(m, rZ)
    m
  }
  def rotX(m:Matrix3f, d:Float) {
    var matX = new Matrix3f()
    matX.rotX(d)
    m.mul(matX)
  }
  def rotY(m:Matrix3f, d:Float) {
    var matY = new Matrix3f()
    matY.rotY(d)
    m.mul(matY)
  }
  def rotZ(m:Matrix3f, d:Float) {
    var matZ = new Matrix3f()
    matZ.rotZ(d)
    m.mul(matZ)
  }
}
